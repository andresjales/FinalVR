﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BallsCollector : MonoBehaviour
{
    public List<BillardBalls> balls;
    int score;
    [SerializeField] TMP_Text scoreText;
    public void RestartGame()
    {
        foreach (BillardBalls item in balls)
        {
            if (item.isActiveAndEnabled)
            {
                item.ResetPosition();
            }
        }
        ResetScore();
        foreach (BillardBalls item in balls)
        {
            item.gameObject.SetActive(true);
        }
    }
    public void AddPoints(BillardBalls ball)
    {
        score += ball.number;
        scoreText.text = score.ToString();
    }
    void ResetScore()
    {
        score = 0;
        scoreText.text = scoreText.text = score.ToString();
    }
}
