﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillBeer : MonoBehaviour
{
    [SerializeField] float fillSpeed;
    float fill;
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Beer"))
        {
            if (other.gameObject.GetComponent<Renderer>().material.GetFloat("_fillValue") < 0.148f)
            {
                fill = other.gameObject.GetComponent<Renderer>().material.GetFloat("_fillValue");
                fill += fillSpeed * Time.deltaTime;
                other.gameObject.GetComponent<Renderer>().material.SetFloat("_fillValue", fill);
                if (fill >= 0.148f)
                {
                    fill = 0.148f;
                }
            }
        }
    }
}
