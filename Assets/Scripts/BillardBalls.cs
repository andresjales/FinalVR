﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillardBalls : MonoBehaviour
{
    public int number;
    [SerializeField] Transform initialPosition;
    public void ResetPosition()
    {
        StartCoroutine(MoveTransform());
        if (number != 0)
        {
            gameObject.SetActive(false);
        }
    }

    IEnumerator MoveTransform()
    {
        GetComponent<Rigidbody>().Sleep();
        transform.position = initialPosition.position;
        yield return new WaitForSeconds(1f);
    }
}
