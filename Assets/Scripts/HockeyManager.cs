﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class HockeyManager : MonoBehaviour
{
    int score1, score2;
    [SerializeField] TMP_Text scoreText1 , scoreText2;
    [SerializeField] PuckBehav puck;

    public void RestartGame()
    {
        score1 = 0;
        score2 = 0;

        scoreText1.text = score1.ToString();
        scoreText2.text = score2.ToString();

        puck.ResetPuck();
    }

    public void AddPointsPlayer1()
    {
        score1++;
        scoreText1.text = score1.ToString();
    }

    public void AddPointsPlayer2()
    {
        score2++;
        scoreText2.text = score2.ToString();
    }
}
