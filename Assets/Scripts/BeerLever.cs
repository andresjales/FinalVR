﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeerLever : MonoBehaviour
{
    [SerializeField] ParticleSystem filligFX;
    [SerializeField] GameObject beerHose;
    public void PlayFillFX()
    {
        filligFX.Play();
    }
    public void StopFillFX()
    {
        filligFX.Stop();
    }

    public void LeverUp()
    {
        print("Lever Up");
        beerHose.SetActive(false);
        StopFillFX();        
    }
    public void LeverDown()
    {
        print("Lever Down");
        beerHose.SetActive(true);
        PlayFillFX();        
    }
}
