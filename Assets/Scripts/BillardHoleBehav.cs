﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillardHoleBehav : MonoBehaviour
{
    [SerializeField] BallsCollector ballsCollector;
    void OnCollisionEnter(Collision collision)
    {
        if (collision.rigidbody.CompareTag("Ball"))
        {
            collision.gameObject.GetComponent<BillardBalls>().ResetPosition();
            ballsCollector.AddPoints(collision.gameObject.GetComponent<BillardBalls>());
            Debug.Log("Ball #" + collision.gameObject.GetComponent<BillardBalls>().number);
        }
    }

}
