﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuckBehav : MonoBehaviour
{
    [SerializeField] Transform initialPosition;
    [SerializeField] HockeyManager manager;
    public void ResetPuck()
    {
        GetComponent<Rigidbody>().Sleep();
        transform.position = initialPosition.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Hole 1"))
        {
            manager.AddPointsPlayer1();
            ResetPuck();
        }
        else if (other.CompareTag("Hole 2"))
        {
            manager.AddPointsPlayer2();
            ResetPuck();
        }
    }
}
